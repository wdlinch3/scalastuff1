package week3

class Rational( x: Int, y: Int) {
	require( y != 0, "Denominator must be non-zero.")

	private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b )
	
	def numer = x
	def denom = y
	
	//def less(that: Rational): Boolean = numer * that.denom < that.numer * denom 			a better way is
	def < (that: Rational): Boolean = numer * that.denom < that.numer * denom
	
	def max(that: Rational): Rational = if (this < that) that else this
	
	// def add(that: Rational) =		a better way is
	def + (that: Rational) =
		new Rational(
			numer * that.denom + that.numer * denom,
			denom * that.denom )
	
	override def toString = {
		val g = gcd(numer, denom)
		numer/g + "/" + denom/g
	}
	
	//def neg() = new Rational(-numer, denom ) 			// also just neg: Ratoinal =
	def unary_- :Rational = new Rational(-numer, denom )
			
	//def sub(that: Rational) = add(that.neg) 	or better
	//def - (that: Rational) = this + that.neg
	def - (that: Rational) = this + -that
	
}
