package week5

object insort {
  val fruit = List("apples", "oranges", "pears")  //> fruit  : List[java.lang.String] = List(apples, oranges, pears)
  val nums = List(1, 2, 5, 4, 6)                  //> nums  : List[Int] = List(1, 2, 5, 4, 6)
  val diag3 = List(List(1, 0, 0), List(0, 1, 0), List(0, 0, 1))
                                                  //> diag3  : List[List[Int]] = List(List(1, 0, 0), List(0, 1, 0), List(0, 0, 1))
                                                  //| 
  val empty = List()                              //> empty  : List[Nothing] = List()

  val pair = ("answer", 42)                       //> pair  : (java.lang.String, Int) = (answer,42)
  val (label, value) = pair                       //> label  : java.lang.String = answer
                                                  //| value  : Int = 42

  def isort(xs: List[Int]): List[Int] = xs match {
    case List() => List()
    case y :: ys => insert(y, isort(ys))
  }                                               //> isort: (xs: List[Int])List[Int]

  def insert(x: Int, xs: List[Int]): List[Int] = xs match {
    case List() => List(x)
    case y :: ys => if (x <= y) x :: xs else y :: insert(x, ys)
  }                                               //> insert: (x: Int, xs: List[Int])List[Int]

  def init[T](xs: List[T]): List[T] = xs match {
    case List() => throw new Error("List is emply.")
    case List(x) => List()
    case y :: ys => y :: init(ys)
  }                                               //> init: [T](xs: List[T])List[T]

  def concat[T](xs: List[T], ys: List[T]): List[T] = xs match {
    case List() => ys
    case z :: zs => z :: concat[T](zs, ys)
  }                                               //> concat: [T](xs: List[T], ys: List[T])List[T]

  def reverse[T](xs: List[T]): List[T] = xs match {
    case List() => List() // or return xs
    case y :: ys => concat(reverse(ys), List(y)) // or ++
  }                                               //> reverse: [T](xs: List[T])List[T]

  def removeAt(n: Int, xs: List[Int]) = (xs take n) ::: (xs drop n + 1)
                                                  //> removeAt: (n: Int, xs: List[Int])List[Int]

  isort(nums)                                     //> res0: List[Int] = List(1, 2, 4, 5, 6)
  insert(3, nums)                                 //> res1: List[Int] = List(1, 2, 3, 5, 4, 6)
  init(nums)                                      //> res2: List[Int] = List(1, 2, 5, 4)
  concat(nums, nums)                              //> res3: List[Int] = List(1, 2, 5, 4, 6, 1, 2, 5, 4, 6)
  reverse(nums)                                   //> res4: List[Int] = List(6, 4, 5, 2, 1)
}